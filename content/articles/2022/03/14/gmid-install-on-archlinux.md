+++
title = "Gemini Protocol のサーバ実装 gmid を ArchLinux にインストールする"
author = ["Nakaya"]
lastmod = 2022-03-15T13:43:04+09:00
tags = ["archlinux", "gemini-protocol"]
draft = false
+++

## おことわり {#おことわり}

この記事では断りなく[mustache構文](https://mustache.github.io/)を用いることがある。注意されたい。


## Gemini Protocol とは {#gemini-protocol-とは}


### 概要 {#概要}

今の WWW には、問題点があると考える。
広告やトラッキング、自動再生される動画などである。
特に広告とトラッキングに関しては、広告やトラッキングをブロックすることが、ブラウザの機能として取り込まれるまでになり、さらに、それらが欠かせないものとなってしまった。

gemini は、そんなカオスな現代の WWW とは、対照的なプロトコルである。

[公式サイトの FAQ](https://gemini.circumlunar.space/docs/faq.gmi) にある 「what is gemini?
」の章には、大体以下のことが書いてある。

-   プロトコルとしては、OSI 参照モデルにおけるアプリケーション層に該当する
-   プロトコルに加え、軽量なハイパーテキストフォーマットを加えたもの
-   Web を本来のあるべき姿に戻しつつ、 Gopher&nbsp;[^fn:1] を現代風に味付けしたもの。
    -   URI、MIME type や TLS といった、標準化され、成熟している技術の上に成立している
-   シンプルさとプライバシーを念頭に置いている
    -   拡張を困難にすることで、シンプルでプライバシーを確保することができる

その他、Gemini の response は、 HTTP のように Header 部と Body 部に分かれている。
Header 部には status code や、その文書の言語などを記述する、などに用いられている。

gemini に CSS や JavaScript に当たるものはない。CSS 的な装飾は Client に委ねられている。


### gemtext {#gemtext}

先程の箇条書きにあった「軽量なハイパーテキストフォーマット」は gemtext(MIME: text/gemini、拡張子:gmi)と考えられる。

これは Markdown をベースとした軽量マークアップ言語である。
gemini protocol で閲覧できるページのほとんどは、この gemtext で書かれている。
Markdown との大きな差異はリンクにある。

```markdown
Markdownではこのように文中に[リンク]()を挿入することができるが、
```

```gemini
gemtextではこのように

=> https://gemini.circumlunar.space リンク

は改行しなければならない。
```

しかし、これは表現力を落としてでも、パースを簡単にしようとしているのではないかと考えている。

また、gemtext には [Vim](https://tildegit.org/sloum/gemini-vim-syntax)、[Emacs](https://git.carcosa.net/jmcbray/gemini.el) や [nano](https://github.com/yzzyx-network/nanorc/blob/master/gemini.nanorc) など各エディタに syntax highlight が用意されている。


### client {#client}

server に関しては後の章に譲るとして、ここでは client について紹介する。
client について紹介するのは、本筋ではないため、軽く紹介するに留める。
興味のある方は[公式サイトのページ](https://gemini.circumlunar.space/software/)、もしくは [awesome-gemini](https://github.com/kr1sp1n/awesome-gemini)を参照していただきたい。

GUIは[Lagrange](https://gmi.skyjake.fi/lagrange/)、TUIは[Amfora](https://github.com/makeworld-the-better-one/amfora)が、おすすめと挙げられることが多いように感じられる。
ちなみに、Emacs には [Elpher](https://thelambdalab.xyz/elpher/) というクライアントが [melpa](https://melpa.org/#/elpher) から入手できる。


## gmid を選んだ理由 {#gmid-を選んだ理由}

gmid を選んだ理由として 2 つの譲れない点があった。

1.  eniehack.net は、友人とのカンパによって運用されているので、みんなで使えるようにしい
2.  すでに Nextcloud が運用されているマシンなので、ファイルシステムをひっ迫したくない

これらから、要件として以下の基準を満たすものをサーバとして採用しようと考えた:

1.  per-user directory(例: <https://example.com/~username>)がやりたい
2.  Go や Nim などのコンパイラを、サーバに導入せずに済む

これらを満たしたものを[awesome-gemini](https://github.com/kr1sp1n/awesome-gemini)から検討した結果、gmid を使うこととした。

他にも、Nim で作られた [geminim](https://github.com/ardek66/geminim) や、 `inetd` などのスーパーサーバで動作する [vger](https://tildegit.org/solene/vger) などがある。
実装された言語でも、動作する形態でも、バリエーションが豊富なので、是非いろいろ見てほしい。


## gmid のインストール {#gmid-のインストール}


### AUR から {#aur-から}

AUR から `gmid` パッケージをインストールする。
ちなみに、 `gmid-bin` や `gmid-git` もある。

2022/03/15 追記: systemd-sysusers の設定ファイルや systemd の service ファイルの修正が、AUR パッケージの更新により導入された[^fn:2]。
そのため、以下、訂正線の入った作業は行う必要がなくなった。
具体的には、AUR から gmid のインストールが完了したら、「gmid.conf の編集」から再開して問題ない。
2022/03/15 追記終わり

~~また、PKGBUILD を自分で修正したことがある、という上級者は、以下の作業によって `.service` ファイルの編集、gmid ユーザの作成の各章を飛ばすことができる。~~

~~1. `yay -G` や `aur fetch` などを用いて、 `PKGBUILD` を入手~~
~~2. 私の pastebin にある [3 つのファイル](https://pastebin.com/u/eniehack/1/LDUKRB9m)を curl などで入手し、 `PKGBUILD` のあるディレクトリに入れる~~

```shell-session
$ curl -sSL -o gmid.sysusers https://pastebin.com/raw/U9fMpT37
$ curl -sSL -o gmid.service https://pastebin.com/raw/dKfDWpkz
$ curl -sSL -o PKGBUILD.diff https://pastebin.com/raw/MNt0Dcfy
```

~~3. `PKGBUILD` に patch を当てる~~

```shell-session
$ patch PKGBUILD{,.diff}
```

~~4. `makepkg` や `aur build` などを用いて、パッケージを作成し、インストール~~


### service ファイルの編集 {#service-ファイルの編集}

~~このパッケージは systemd の `.service` もインストールされるのだが、2022/03/14 現在、そのままでは動作しないので~~

```shell-session
# systemctl edit gmid
```

~~を実行して~~

```nil
[service]
ExecStart=
ExecStart=/usr/bin/gmid -f -c /etc/gmid.conf
```

~~と書き加える必要がある。~~


### gmid ユーザの作成 {#gmid-ユーザの作成}

~~公式ドキュメントでは gmid を実行するためのユーザを作成することを勧めているので作成する。~~

~~ArchLinux には、system user を作成するためのツールが systemd によって用意されているので[^fn:3]、これを使う。~~

~~gmid の upstream が、新しいリリースを出したら、AUR パッケージにも反映されるはずである。 `/usr/lib/sysusers.d/gmid.conf` がなければ、curl で取得する&nbsp;[^fn:4] 。~~

```shell-session
$ curl -sSL -o gmid.conf https://raw.githubusercontent.com/omar-polo/gmid/master/contrib/gmid.sysusers
```

~~以下のコマンドを実行すれば作成できる:~~

```shell-session
# systemd-sysusers ./gmid.conf
```

~~念のために `/etc/passwd` を見ておいてもいいかもしれない。~~


### gmid.conf の編集 {#gmid-dot-conf-の編集}

[gmid公式ドキュメントのquickstart](https://gmid.omarpolo.com/quickstart.html)を参考に `gmid.conf` を設定する。
ちなみに、gmid パッケージには `gmid.conf` 向け syntax highlight 用の vim script も付属している。

例えば、per-user directory を作りたい場合:

```nil
server "{{FQDN}}" {
    cert "{{public key path}}"
    key "{{private key path}}"

    root "/usr/local/share/gmid/"

    location "/~*" {
        root "/usr/local/share/gmid"
    }
}
```

などとする。

pacman が、 `/usr/local`  以下に手を出すことは基本的にありえない[^fn:5]。
そのため、私はこのディレクトリにしたというだけである。
なので、location ディレクティブに記述する directory path は、都合に応じて適宜変更して問題はないはずである。


### gencert の実行 {#gencert-の実行}

理由はよくわからないのだが、Gemini を介して公開されているサーバは、自己署名証明書で TLS 通信を行っている。
そのため、gmid についてくる `gencert` という Perl 製ヘルパースクリプトを実行する。

```shell-session
# /usr/share/gmid/gencert {{FQDN}}
```

その後、 `gmid.conf` に記述した場所へ公開鍵(拡張子: `.pem` )、秘密鍵(拡張子: `.key`)を移動する。
その際、秘密鍵を入れておくディレクトリは、必ず `chmod` で 700 に設定しておくこと。


### firewall の設定見直し {#firewall-の設定見直し}

最後に `ufw` などの firewall の設定を変更する。
gemini protocol のデフォルトポートは 1965 であるので、以下を実行する:

```shell-session
# ufw allow 1965
```


### service 実行 {#service-実行}

```shell-session
# systemctl start gmid
```


## こぼれ話 {#こぼれ話}

gmid の upstream には以下が付属している:

-   証明書管理用のヘルパスクリプト
-   config ファイルを syntax highlight するための、vimscript
-   systemd の service ファイル
-   systemd-sysusers のための config ファイル

しかし、そのほとんどが AUR のパッケージではインストールされていなかった。
そのため、AUR に patch を送ったのは初めてのことだった。
すべてが取り入れられたわけではなかった。

しかし、取り入れられなかった点は upstream へ Pull Request を投げたらどうかと言われたため、[PR](https://github.com/omar-polo/gmid/pull/14)を投げたところ、すんなり受け入れられてしまった。

また、Gemini と同様、HTTP でも、いにしえの \`<https://eniehack.net/~eniehack>\` のような URL で自己紹介を書いている。
そのため、gemtext から HTML へ変換するプログラムなどを用いて、gemtext から HTML が生成されると便利かもしれない。


## 参考 {#参考}

-   [Gemini Protocol公式サイト](https://gemini.circumlunar.space/)

[^fn:1]: 詳しくは[Archwiki](https://wiki.archlinux.jp/index.php/Gopher)、または[Wikipedia](https://ja.wikipedia.org/wiki/Gopher)を参照のこと。
[^fn:2]: <https://aur.archlinux.org/cgit/aur.git/commit/?h=gmid&id=8432689b101b93366fcb6a82259503a6f6a0396c> を参照。
[^fn:3]: systemd-sysusers。詳細は man page:[systemd-sysusers(8)](https://man.archlinux.org/man/systemd-sysusers.8)を参照。
[^fn:4]: 2022/03/14 現在、まだリリースされていない。
[^fn:5]: [Arch パッケージガイドライン](https://wiki.archlinux.jp/index.php/Arch%5F%E3%83%91%E3%83%83%E3%82%B1%E3%83%BC%E3%82%B8%E3%82%AC%E3%82%A4%E3%83%89%E3%83%A9%E3%82%A4%E3%83%B3#.E3.83.91.E3.83.83.E3.82.B1.E3.83.BC.E3.82.B8.E3.82.A8.E3.83.81.E3.82.B1.E3.83.83.E3.83.88)を参照。
