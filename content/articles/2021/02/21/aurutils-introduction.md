+++
title = "yayからaurutilsに移行してみる"
author = ["Nakaya"]
lastmod = 2021-02-21T22:40:23+09:00
tags = ["archlinux", "bash", "product", "software"]
draft = false
+++

## 動機 {#motivation}

AURヘルパーは今までyayを使っていて、何ら不自由はなかった。しかし、書籍『UNIXという考え方』やPOSIX原理主義に感銘を受けたというのが大きい。特に前者の書籍にあった「実行速度より可搬性」というのはかなり同意できた。

shellscriptの良さはPOSIX準拠である環境であれば、確実に使える点だ。これはPerl、PythonやRubyにはない有利な点である。また、shellscriptは遅いと一般的には考えられているが、パイプなどを意識すれば、shellsciptは高速化可能であることも[^fn:1]、shellscriptのよい点だ。

そのような思想に影響を受けたなか、bash製のaurutilsというAURヘルパーを発見し、私は使っていたyayからaurutilsへ移行してみることにした。


## 対象読者 {#target-readers}

ArchLinux系のディストリビューションを使うユーザをこの記事は対象とする。ただし、その中でもTerminalに触れることに抵抗がなく、AURについて知っていることを前提として話を進めることとする。


## aurutilsとは {#what-is-aurutils}

aurutilsとはbash製のAURヘルパである。

AURヘルパにもgolang製のyay、nim製のpakkuなど、様々なものがある。しかし、それらとは異なり、このaurutilsはpacmanにあるcustom local repositoryという機能[^fn:2]を用いて、AURにあるパッケージをインストールする。

具体的に、custom local repositoryを使わないyayと、使うaurutilsではどのように違うのか、比較してみる。
yayなどでは、以下のような動作をする。

1.  AURからパッケージを取得
2.  依存関係の解決
3.  パッケージのインストールを行う。（ `makepkg -si` ）

しかし、aurutilsでは

1.  AURからパッケージを取得
2.  依存関係の解決
3.  ビルドでちらかった環境の整理
4.  所定の位置へアーカイブを配置する

だけだ。インストールはaurutilsが行なわない。これは、公式リポジトリのパッケージのように、pacmanが行う。


### 特徴 {#merit}

yay-binを使えば話は別だが、yayはgolangのコンパイラを導入しなければならない。しかし、aurutilsは幾つかのbashスクリプトと、幾つかのmanドキュメントで構成されており、パッケージサイズが小さい。実際に、パッケージアーカイブ( `pkg.tar.zst` ）の状態で、yay-binは約8MB、aurutilsは約100KBである。
yayのほうが、依存するパッケージは少ないかもしれないが、aurutilsでは依存するパッケージの1つ1つが、小さい。

加えて、aurutilsに関してはpaccacheも設定なしにcustomリポジトリに適用できた。しかし、 `makepkg.conf` に `PKGDEST=/path/to/yay/cache` のように設定し、 `paccache -c /path/to/yay/cache` のように実行すれば、yayでも実行できる。しかし設定せずとも、paccacheが使える点では、yayより有利かもしれない。

ちなみに、paccacheは[pacman-contrib](https://archlinux.org/packages/community/x86%5F64/pacman-contrib/)パッケージにあるコマンドだ。localにあるのパッケージアーカイブを、デフォルトで最新の3つのみを残して、それ以外を削除することができる。

ただ、aurutilsは `yay -Ss` のような、AURと公式リポジトリを横断的に探索する機能がない、という不便な部分もある。

これらの特徴を考えると、サーバや、HDDの小さいPCなど、ディスクをシビアに管理しなければならないような環境に向いているのかもしれない。


### 使い方 {#aurutils-usage}

aurutilsはaurコマンドと、いくつかのサブコマンドで構成されている。サブコマンドの詳しい解説は `man 1 aur-sync` のように、manコマンドからそれぞれ読むことができる。


#### install {#install}

AURからパッケージをインストールするときは、まず、 `aur sync $pkgname` のようにパッケージをビルドし、配置する。次に、データベースを更新して（ `pacman -Syy` ）、インストール（ `pacman -S $pkgname` ）する。


#### update {#update}

aurutilsを用いてAURから取得したパッケージの更新は、 `aur sync -u` を実行する。このコマンドによって、まず、パッケージの更新確認する。その結果から、更新がある場合はビルドしてcustomリポジトリへ配置する。


#### search {#search}

パッケージの検索は `aur search $pkgname` のようにコマンドを実行する。


#### その他 {#その他}

その他にもaurutilsは `aur` コマンドのサブコマンドとして様々な機能を提供している。

-   aur build

パッケージをビルドする

-   aur fetch

パッケージを取得してくる

-   aur chroot
    `systemd-nspawn` を用いてパッケージをビルドする

-   aur query

    aurwebへGETリクエストを送る

-   aur pkglist
    AURにある全てのパッケージ名を取得する


## 導入 {#install}

ここからは、yayからaurutilsへ実際に移行してみることにする。[^fn:3]を参考にした。また、検証はしていないものの、yay以外の既存のAURヘルパーに関しても、パッケージアーカイブを移動し、 `repo-add` コマンドでパッケージを登録すればよいと思われる。さらに、AURヘルパーを初めて導入する場合は、[^fn:3]とここを参考にすれば導入できるはずだ。

1.  `pacman -Qqm > ~/aurpkglist`
2.  パッケージアーカイブを移動する

    [yayからlocal repoへアーカイブ(pkg.tar.(xz|zst))を移動する自作scriptを書いた](https://gitlab.com/-/snippets/2076763)ので参考にどうぞ。

    ちなみに、私は `/etc/makepkg.conf` の `PKGEXT` を `".pkg.tar.zst"` にしている[^fn:4]。なぜなら、ZStandardは展開と圧縮がgzipより高速で、かつpacmanがデフォルトで用いている圧縮フォーマットだからだ。

3.  pacman.confの編集

作成するcustomリポジトリをpacmanへ認識させる。
`/etc/pacman.d/custom/` へ 以下の内容を書きこみ、

```nil
[options]
CacheDir = /var/cache/pacman/pkg
CacheDir = /var/cache/pacman/custom
CleanMethod = KeepCurrent

[custom]
SigLevel = Optional TrustAll
Server = file:///var/cache/pacman/custom
```

`/etc/pacman.conf` の最後に `Include = /etc/pacman.d/custom/` を追記する。

1.  `sudo install -d /var/cache/pacman/custom -o $USER` でディレクトリを作成する
2.  `cd /var/cache/pacman/custom/`
3.  `repo-add custom.db.tar`

    `repo-add` とはpacmanが提供する個人的なリポジトリを管理するコマンドだ。このコマンドでは、ワイルドカードでパッケージアーカイブを指定できる。ここではcustomリポジトリのデータベースを作成している。

4.  `repo-add -n custom.db.tar *.pkg.tar.xz`

    ここではcustomリポジトリのデータベースにyayが作成したパッケージアーカイブを登録する。この作業は長いので、ここでティーブレイクをしてもいいだろう。

5.  最後に `pacman -Syu` をすれば完了だ。


## 最後に {#conclusion}

custom local repositoryを始めて知った。これを応用して、gemやpipのパッケージを管理するのはよさそうだ、と思った。

最後に。添削に協力してくださった、skiaさん、trash\_canさんありがとうございました。この場で感謝いたします。


## 参考 {#参考}

-   公式GitHubリポジトリ: [AladW / aurutils - GitHub](https://github.com/AladW/aurutils)

[^fn:1]: 松浦 智之、大野 浩之、當仲 寛哲『ソフトウェアの高い互換性と長い持続性を目指すPOSIX中心主義プログラミング』<https://www.ipsj.or.jp/dp/contents/publication/32/S0804-R1601.html>より。2021/02/20 閲覧。
[^fn:2]: [Pacman/Tips and tricks#Custom local repository - Archwiki](https://wiki.archlinux.org/index.php/Pacman/Tips%5Fand%5Ftricks#Custom%5Flocal%5Frepository)
[^fn:3]: [Arch Linux: aurutils installation and configuration - GitHub Gist](https://gist.github.com/geosharma/afe1ea9ebe58cb67aaaba62a0d47bc7a)
[^fn:4]: 詳細は `man 5 makepkg.conf` もしくは <https://man.archlinux.org/man/makepkg.conf.5.en>を参照のこと
