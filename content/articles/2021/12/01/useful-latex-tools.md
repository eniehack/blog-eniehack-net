+++
title = "LaTeX にまつわる便利ツールを紹介する"
author = ["Nakaya"]
lastmod = 2021-12-01T00:44:08+09:00
tags = ["latex", "advent-calendar"]
draft = false
+++

この記事は [群馬高専アドベントカレンダー2021](https://adventar.org/calendars/6424) 1 日目の記事です。

私がこの記事で言いたいのは LaTeX をカジュアルに使おうぜ、ということです。

E 科では 2 年の授業で、J 科では 3 年の実験で学ぶ LaTeX ですが、レポートや卒業研究とお堅いシチュエーションで使うことが多く、また面倒だと思われがち(？)です。
しかし、数式の出力が綺麗であったり、pdf を生成する言語として優秀です。
もっと日常的に使ってもいいのではないか？と思っています。

そこで、私が実際に使っている、LaTeX に関する便利ツールやパッケージについて紹介しようと思います。


## 自己紹介 {#自己紹介}

遅れました。5 年 E 科の Nakaya と申します。

電算部で[群馬高専 IT勉強会](https://nitgclt.connpass.com/)の主催や、工華祭のポスターレイアウトと [Web サイト](https://22nd.kokasai.com/)開発を大分昔にやっていました。

最近では、工華祭でミニ FM の運営や CPU の自作をやってます。


## 対象読者 {#対象読者}

早速、記事の本題に移ります。この記事の対象読者は LaTeX が難なく使える人とします。
TeX を TeX Live でインストールした人を主に対象としますが、インストール方法に変わりがあるのみなので、調べてみてください。


## 便利ツール {#便利ツール}


### cluttex {#cluttex}

[cluttex](https://blog.miz-ar.info/2018/10/cluttex-release/) とはコンパイルを自動化するコマンドラインツールです。

.aux や.log などのファイルを別ディレクトリで行うため、使っているディレクトリが汚れません。
相互参照などの必要に応じ、複数回のコンパイルを自動で行ってくれます。

最近の TeXLive では cluttex がデフォルトで入っているようですが、入っていない場合は TeX Live を更新しましょう。

cluttex の使い方としては、TeX ファイルから PDF を生成する際に、以下のように実行します。

```nil
cluttex -e lualatex hogehoge.tex
```

`-e` で使っている LaTeX のエンジンを指定します。
jsarticle を利用している場合は 基本的には platex とします。

また、`--watch`でファイルが保存された都度コンパイルしてくれるようになります。


### pandoc {#pandoc}

[pandoc](https://pandoc.org) とはマークアップ言語間の変換を行うコマンドラインツールです。
ちなみに、マークアップ言語とは、見出し、リンク、太字、リストなどの文章中の要素を文字によって文書を表現するの言語です。

pandoc が対応しているフォーマットの、HTML、Markdown、LaTeX、Word(.docx)、LibreOffice Writer(.odt)などを一部のフォーマットを除き、基本的には相互変換することができます。
それに加え、LaTeX を介して HTML、Markdown などから PDF を生成することもできます。

導入するには、公式サイトの[ダウンロードページ](https://pandoc.org/installing.html)からインストーラが入手できます。
また、Windows 以外の OS での入手方法は上のページに案内があります、

markdown から latex へ変換する際は、以下のように実行します。

```nil
pandoc input.md -f markdown -t latex -s -o output.pdf
```

`-f`で入力する文書フォーマットを、`-t`で出力するフォーマットを指定します。しかし、pandoc は拡張子から判別できため、実際は省略できます。
`-o`で出力するファイルの先を指定します。


## パッケージ {#パッケージ}


### here {#here}

here パッケージは図や表の場所を指定するパッケージです。
`figure`、`table`のオプションとして`[!hbtp]`と指定することが多いと思います。しかし、思った場所に図表を出力してくれない場合があります。そこで使えるのが here というわけです。

使い方としては、

```nil
\begin{figure}[H]
\end{figure}
```

このようにオプションに `[H]` と指定するだけで、ページの残りスペースが足らないと改ページされてしまいますが、意図した場所に出力してくれます。


### beamer {#beamer}

LaTeX でスライドが書けることはご存知でしょうか？
LaTeX でスライドを書けるドキュメントクラスでも、最も有名な beamer です。

beamer を使って、スライドを書いてみたサンプルが[こちら](https://speakerdeck.com/eniehack/crystalhaiizo)です。
テーマは[Metropolis](https://github.com/matze/mtheme)を使いました。
このようにモダンなテーマもあります。

[このページ](https://qiita.com/htlsne/items/70cbb488e7a87cd9e228)にモダンな beamer のテーマ集があります。気になった方は見てみるといかもしれません。


## さいごに {#さいごに}

日本語で TeX の情報を集める際、[TeX Wiki](https://texwiki.texjp.org/)がとても役に立ちます。
今紹介したツールやパッケージはもちろん、「頻発するエラーと対処集」なるページまであります。是非見てください。

[群馬高専アドベントカレンダー2021](https://adventar.org/calendars/6424) 明日は、はいからさんの「高校と高専と大学と、教養教育と専門教育について」です。
