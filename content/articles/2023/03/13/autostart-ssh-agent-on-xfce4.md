+++
title = "xfce4 ではデフォルトで ssh-agent を自動起動するらしい"
author = ["Nakaya"]
lastmod = 2023-03-13T13:57:37+09:00
tags = ["ssh-agent", "gpg-agent", "archlinux", "xfce4"]
draft = false
+++

## TL;DR {#tl-dr}

xfce4 では、デフォルトで ssh-agent を自動起動する設定になっているので、設定エディタや Terminal から無効化する必要がある。
Terminal から実行する場合は以下のように実行する:

```shell
 xfconf-query -c xfce4-session -p /startup/ssh-agent/enabled -n -t bool -s false
```


## 経緯 {#経緯}

最近は gpg に日常を大きく依存する日々を送っている。というのも、pgp 鍵はかなり便利であるからだ。

例えば以下のものが挙げられる:

-   password manager の [pass](https://www.passwordstore.org/) で master password として使う
-   副鍵に Authentication の権限を持たせて SSH 鍵として使う
-   pgp 鍵の notation 機能を使って [Keyoxide](https://keyoxide.org/) で keybase もどきをする


## 本題 {#本題}

これほど gpg に頼って生活を送っていると、毎回 gpg のパスワードを入力するのがバカらしくなってくる。

ここで、Linux 環境の gpg に備えられている gpg-agent が登場する。これを使えば、ssh-agent のように SSH でサーバにアクセスする際、鍵の passphrase を要求される回数が大きく減ってうれしい。

しかし、ここ数日、ArtixLinux の FS が壊れたので、 PC に ArchLinux をインストールして、久々の xfce4 を楽しんでいた。

もちろん、gpg-agent は QoL に関わる大事なものなのでセットアップを試みようとするも、立ち上げた記憶のない ssh-agent が毎ログインのたびに起動してくる。

悪く言えば ssh-agent をジャックする gpg-agent は、ssh-agent が後に起動してしまうと、例え gpg-agent が立ち上がったとしても利用することができない。

このままだとつらいね、ということで、数日間悪戦苦闘して直したので、数日の供養として、解決しなかった手法、した手法をそれぞれ述べていこうと思う。


## この記事の対象 {#この記事の対象}

signin するだけで gnome-keyring の Default keyring が開く、であったり、signin すると pgp 鍵が開く、といった gnome-keyring と gpg-agent の環境構築は、対象としない。これに関しては ArchWiki の [gnome-keyring](https://wiki.archlinux.org/title/GNOME/Keyring) と [gpg](https://wiki.archlinux.org/title/GnuPG#gpg-agent) のページを見るほうが手っ取り早い。

代わりに、この記事は、xfce4 において、勝手に ssh-agent が起動されてしまう問題の解決を提示する。


## 解決しなかった {#解決しなかった}


### systemd の environment.d を使う {#systemd-の-environment-dot-d-を使う}

ユーザのログイン時に systemd が参照する環境変数は.bashrc や.xinitrc と独立であるらしい。その環境変数をユーザが操作するには、  `~/.config/environment.d/*.conf` を編集すればよいらしく[^fn:1]、実際に  `.xinitrc` などの概念がない Wayland では利用されているらしい[^fn:2]。

しかし、この手法では、解決しなかった。どうやら、このファイルには shellscript における  `unset` が用意されておらず、構文エラーとなってしまった。


### systemd の environment generator を使う {#systemd-の-environment-generator-を使う}

また、systemd には、先程の environment.d のファイルを生成するための機構がある&nbsp;[^fn:3]。ユーザごとの環境変数であれば、 `/etc/systemd/user-environment-generators/` にプログラムを格納し、標準出力で environment.d 形式の要素を吐ければよいらしい。これは、言語は問われないので、shellscript である必要は全然なく、Python などで書いても問題ないらしい。

こちらも解決はしなかった。そもそも、こちらにも shellscript における  `unset` が用意されていないようだ。


### gnome-keyring などの systemd user unit を無効化する {#gnome-keyring-などの-systemd-user-unit-を無効化する}

確かに `gcr-ssh-agent.service` は無効化する意味があるのだが[^fn:4]、他の service ファイルや socket ファイルを無効化したところで ssh-agent を起動するものはないので、無効化したところで解決しないことは自明であった……。


## 解決した {#解決した}

なんと、xfce4 では ssh-agent と gpg-agent を自動起動する機能があるらしい[^fn:5]。なので、ssh-agent を無効化する:

```shell
 xfconf-query -c xfce4-session -p /startup/ssh-agent/enabled -n -t bool -s false
```

ssh-agent を無効化するだけも問題ないのだが、私としては、gnome-keyring を使って gpg-agent を管理したいので、gpg-agent も無効化しておく:

```shell
 xfconf-query -c xfce4-session -p /startup/gpg-agent/enabled -n -t bool -s false
```

これを実行してから、ArchWiki の gpg と gnome-keyring のページにある記述を順番にこなせば、gpg-agent は動作するようになるハズである。

[^fn:1]: <https://man.archlinux.org/man/environment.d.5>
[^fn:2]: <https://wiki.archlinux.jp/index.php/%E7%92%B0%E5%A2%83%E5%A4%89%E6%95%B0#Wayland_.E3.82.BB.E3.83.83.E3.82.B7.E3.83.A7.E3.83.B3.E3.81.94.E3.81.A8>
[^fn:3]: <https://man.archlinux.org/man/systemd.environment-generator.7>
[^fn:4]: <https://wiki.archlinux.jp/index.php/GNOME/Keyring#.E3.82.AD.E3.83.BC.E3.83.AA.E3.83.B3.E3.82.B0_ssh_.E3.82.B3.E3.83.B3.E3.83.9D.E3.83.BC.E3.83.8D.E3.83.B3.E3.83.88.E3.82.92.E6.9C.89.E5.8A.B9.E3.81.AB.E3.81.99.E3.82.8B>
[^fn:5]: <https://docs.xfce.org/xfce/xfce4-session/advanced>